class Message < ActiveRecord::Base
  belongs_to :author, class_name: 'User', counter_cache: true, inverse_of: :messages, foreign_key: :user_id
  belongs_to :chat
  has_and_belongs_to_many :read_by_users, class_name: 'User', join_table: 'users_messages'
  validates :author, presence: true

  def read_by_user(user)
    read_by_users << user
  end
end