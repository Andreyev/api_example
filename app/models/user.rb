class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable

  validates :name, uniqueness: true

  has_many :messages, inverse_of: :author, dependent: :destroy
  has_and_belongs_to_many :chats
  has_and_belongs_to_many :read_messages, class_name: 'Message', join_table: 'users_messages'
  scope :by_user, ->(user) {where id: user}

  private

  def email_required?
    false
  end
end
