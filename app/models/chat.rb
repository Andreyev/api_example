class Chat < ActiveRecord::Base
  has_and_belongs_to_many :users

  has_many :messages, dependent: :destroy
  has_many :read_messages, -> { joins(:read_by_users) }, class_name: 'Message'

  accepts_nested_attributes_for :messages
  validates :users, length: { minimum: 2 }

  def read_by_user(user)
    user.read_message_ids = (user.read_message_ids + message_ids).uniq
  end

  def unread_messages_by_user(user)
    messages.where.not(id: read_messages.merge(User.by_user user))
  end

  def read_messages_by_user(user)
    read_messages.merge User.by_user(user)
  end
end
