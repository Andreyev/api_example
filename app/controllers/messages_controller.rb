class MessagesController < ApplicationController
  before_action :get_chat
  before_action :authenticate_user!
  before_action :set_message, only: [:show, :update, :destroy]

  respond_to :json

  def index
    authorize Message
    @messages = @chat.messages
    respond_with(@messages)
  end

  def unread_messages
    authorize Message
    @messages = @chat.unread_messages_by_user current_user
    respond_with(@messages)
  end

  def show
    authorize @message
    respond_with(@message)
  end

  def create
    @message = @chat.messages.new(message_params)
    @message.author = current_user
    authorize @message
    @message.save
    respond_with(@message)
  end

  def update
    authorize @message
    @message.update(message_params)
    respond_with(@message)
  end

  def destroy
    authorize @message
    @message.destroy
    respond_with(@message)
  end

  private
    def set_message
      @message = @chat.message.find(params[:id])
    end

    def message_params
      params.require(:message).permit(:text)
    end

    def get_chat
      @chat = Chat.find(params.require :chat_id)
    end
end
