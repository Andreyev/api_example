require "application_responder"

class ApplicationController < ActionController::Base
  include Pundit
  self.responder = ApplicationResponder
  respond_to :json

  rescue_from ActiveRecord::RecordNotFound do |exception|
    errors << exception.message
    render 'layouts/error', status: 422
  end

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    errors << t(:not_authorized)
    render 'layouts/error', status: 403
  end

  def errors
    @errors||=[]
  end

  # respond_to :html

  # before_action -> { binding.pry }

  # def current_user
  #   #This is a STUB
  #   super || User.all.first
  # end


  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception

end
