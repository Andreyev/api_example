class ChatsController < ApplicationController
  before_action :set_chat, only: [:show, :update, :destroy, :mark_as_read]
  before_action :authenticate_user!, except: [:index, :show]

  respond_to :json

  def index
    authorize Chat
    @chats = policy_scope(Chat.all.includes(:users))
    respond_with(@chats)
  end

  def show
    authorize @chat
    respond_with(@chat)
  end

  def create
    @chat = Chat.new(chat_params)
    @chat.users << current_user unless @chat.users.include? current_user
    authorize @chat
    @chat.save
    respond_with(@chat)
  end

  def update
    authorize @chat
    @chat.update(chat_params)
    respond_with(@chat)
  end

  def destroy
    authorize @chat
    @chat.destroy
    respond_with(@chat)
  end

  def mark_as_read
    authorize @chat
    @chat.read_by_user current_user
  end

  private

  def set_chat
    @chat = Chat.find(params[:id])
  end

  def chat_params
    params.require(:chat).permit(:name, user_ids: [])
  end

end
