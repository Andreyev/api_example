json.extract! chat, :id, :name, :created_at, :updated_at, :users
json.messages chat_messages_path(chat_id: chat.id, format: :json)
json.unread_messages unread_messages_chat_messages_url(chat, format: :json)
