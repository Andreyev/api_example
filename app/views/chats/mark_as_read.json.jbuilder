json.partial! 'show', chat: @chat
json.array! @chat.read_messages_by_user(current_user) do |message|
  json.partial! 'messages/show', message: message
end