json.array!(@chats) do |chat|
  json.partial! 'show', chat: chat
  json.url chat_url(chat, format: :json)
end
