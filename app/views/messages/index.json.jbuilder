json.array!(@messages) do |message|
  json.partial! 'show', message: message
  json.url chat_message_url(@chat, message, format: :json)
end
