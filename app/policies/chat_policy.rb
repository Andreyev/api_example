class ChatPolicy < ApplicationPolicy

  def mark_as_read?
    user_in_chat?
  end

  def index?
    true
  end

  def create?
    user_in_chat?
  end

  def show?
    user_in_chat?
  end

  def update?
    user_in_chat?
  end

  def destroy?
    user_in_chat?
  end

  class Scope < Scope
    def resolve
      scope.joins(:users).where(users: {id: user})
    end
  end

  private

  def user_in_chat?
    record.user_ids.include? user.id
  end
end
