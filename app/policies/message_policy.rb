class MessagePolicy < ApplicationPolicy

  def unread_messages?
    true
  end

  def index?
    true
  end

  def create?
    has_access_to_chat?
  end

  def show?
    has_access_to_chat?
  end

  def update?
    record.author_id == user.id
  end

  def destroy?
    record.author_id == user.id
  end

  class Scope < Scope
    def resolve
      scope
    end
  end

  private

  def has_access_to_chat?
    ChatPolicy.new(user, record.chat).update?
  end
end
