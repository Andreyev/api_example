class AddMessagesCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :messages_count, :integer
  end
end
