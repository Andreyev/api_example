class AddChatsUsers < ActiveRecord::Migration
  def change
    create_table :chats_users do |t|
      t.references :user, index: true, foreign_key: true
      t.references :chat, index: true, foreign_key: true
    end
  end
end
