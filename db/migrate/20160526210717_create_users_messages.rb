class CreateUsersMessages < ActiveRecord::Migration
  def change
    create_table :users_messages do |t|
      t.references :user, index: true, foreign_key: true
      t.references :message, index: true, foreign_key: true
    end
  end
end
