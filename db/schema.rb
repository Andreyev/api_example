# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160526210717) do

  create_table "chats", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "chats_users", force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "chat_id", limit: 4
  end

  add_index "chats_users", ["chat_id"], name: "index_chats_users_on_chat_id", using: :btree
  add_index "chats_users", ["user_id"], name: "index_chats_users_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "chat_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.text     "text",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["chat_id"], name: "index_messages_on_chat_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "encrypted_password", limit: 255, default: "", null: false
    t.string   "name",               limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "messages_count",     limit: 4
  end

  add_index "users", ["name"], name: "index_users_on_name", unique: true, using: :btree

  create_table "users_messages", force: :cascade do |t|
    t.integer "user_id",    limit: 4
    t.integer "message_id", limit: 4
  end

  add_index "users_messages", ["message_id"], name: "index_users_messages_on_message_id", using: :btree
  add_index "users_messages", ["user_id"], name: "index_users_messages_on_user_id", using: :btree

  add_foreign_key "chats_users", "chats"
  add_foreign_key "chats_users", "users"
  add_foreign_key "messages", "chats"
  add_foreign_key "messages", "users"
  add_foreign_key "users_messages", "messages"
  add_foreign_key "users_messages", "users"
end
